# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

from django.contrib import admin
from django.utils.html import mark_safe, strip_tags

from parler.admin import TranslatableAdmin
from reversion_compare.admin import CompareVersionAdmin
from mywebsite.utils.mixins import TranslateableReversionAdminMixin

from .forms import ProjectAdminForm, WorkItemAdminForm
from .models import Milestone, Project, WorkItem


@admin.register(Milestone)
class MilestoneAdmin(TranslateableReversionAdminMixin, CompareVersionAdmin, TranslatableAdmin):
    fieldsets = (
        (None, {
            'fields': ('title', 'project', 'state', 'duedate'),
        }),
    )
    list_display = ('title', 'duedate', 'project', 'state')
    search_fields = ['translations__title']
    list_filter = ('project', 'state')


@admin.register(WorkItem)
class WorkItemAdmin(TranslateableReversionAdminMixin, CompareVersionAdmin, TranslatableAdmin):
    form = WorkItemAdminForm
    fieldsets = (
        (None, {
            'fields': ('name', 'milestone', 'state', 'date', 'description'),
        }),
    )
    list_display = ('name', 'date', 'list_description', 'milestone', 'state')
    search_fields = ['translations__name', 'translations__description']
    list_filter = ('milestone', 'state')

    def list_description(self, obj):
        return mark_safe(strip_tags(obj.description))


@admin.register(Project)
class ProjectAdmin(TranslateableReversionAdminMixin, CompareVersionAdmin, TranslatableAdmin):
    form = ProjectAdminForm
    fieldsets = (
        (None, {
            'fields': ('title', 'slug', 'code_repository', 'language', 'date', 'description'),
        }),
    )
    list_display = ('title', 'list_description', 'date', 'code_repository')
    search_fields = ['translations__title', 'translations__description']

    def list_description(self, obj):
        return mark_safe(strip_tags(obj.description))
