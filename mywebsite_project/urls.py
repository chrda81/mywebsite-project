# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

from django.urls import include, path

from . import views

app_name = 'project'

urlpatterns = [
    path('', views.ProjectListView.as_view(), name='projects'),
    path('manual', views.manual, name='project-manual'),
    path('<slug:slug>', views.ProjectDetailView.as_view(), name='project-detail'),
]
