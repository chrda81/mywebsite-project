# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class ProjectConfig(AppConfig):
    """The default AppConfig for the website module ``mywebsite_project``."""
    name = 'mywebsite_project'
    verbose_name = _("Project")

    def ready(self):
        """Wire up the signals """
        import mywebsite_project.signals
