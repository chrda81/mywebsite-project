# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

from django.shortcuts import render
from django.views import generic

from .models import Project


class ProjectListView(generic.ListView):
    model = Project
    context_object_name = 'project_list'
    template_name = 'project/project_list.html'
    paginate_by = 5

    def get_queryset(self):
        queryset = super(ProjectListView, self).get_queryset()
        # standard filter
        queryset = queryset.active_translations().distinct().all().order_by('-date')
        return queryset

    def get_context_data(self, **kwargs):
        context = super(ProjectListView, self).get_context_data(**kwargs)
        return context


class ProjectDetailView(generic.DetailView):
    context_object_name = 'project'
    template_name = 'project/project.html'
    model = Project

    def get_context_data(self, **kwargs):
        context = super(ProjectDetailView, self).get_context_data(**kwargs)
        return context


def manual(request):
    return render(request, 'project/project_manual.html')
