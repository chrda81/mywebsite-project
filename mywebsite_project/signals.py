# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

from django.db.models.signals import pre_save
from django.dispatch import receiver

from mywebsite.utils.tools import unique_slug_generator

from .models import Project


# subscribe to event pre_save of table 'project', to create unique slug
@receiver(pre_save, sender=Project)
def pre_save_project_receiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = unique_slug_generator(instance)
