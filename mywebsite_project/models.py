# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

from django.db import models
from django.urls import reverse
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _

from parler.models import TranslatableModel, TranslatedFields
from tinymce.models import HTMLField

# Define states for field 'state'
BLUE_STATE = 0
YELLOW_STATE = 1
GREEN_STATE = 2
STATUS_CHOICES = (
    (BLUE_STATE, 'No working activities'),
    (YELLOW_STATE, 'Work in progress'),
    (GREEN_STATE, 'Work completed'),
)


# Hint: Making existing fields translatable,
# see https://django-parler.readthedocs.io/en/latest/advanced/migrating.html
class Project(TranslatableModel):
    translations = TranslatedFields(
        title=models.CharField(max_length=140),
        description=HTMLField(default='')
    )
    date = models.DateField(default=now)
    code_repository = models.CharField(max_length=140, default='')
    language = models.CharField(max_length=80)
    slug = models.SlugField(max_length=255, editable=True, unique=True, db_index=True, default=None, blank=True)

    class Meta:
        db_table = 'project_project'
        verbose_name = _('Project')
        verbose_name_plural = _('Projects')

    def __unicode__(self):
        if self.language_code:
            return self.title
        else:
            return '{} (ID: {})'.format(self.__class__.__name__, self.pk)

    def __str__(self):
        if self.language_code:
            return self.title
        else:
            return '{} (ID: {})'.format(self.__class__.__name__, self.pk)

    def get_absolute_url(self):
        return reverse('project', kwargs={'slug': self.slug, 'pk': self.pk})


# Hint: Making existing fields translatable,
# see https://django-parler.readthedocs.io/en/latest/advanced/migrating.html
class Milestone(TranslatableModel):
    translations = TranslatedFields(
        title=models.CharField(max_length=140)
    )
    duedate = models.DateField(default=now)
    project = models.ForeignKey('Project', on_delete=models.CASCADE, related_name="milestones")
    state = models.IntegerField(choices=STATUS_CHOICES, default=0)

    class Meta:
        db_table = 'project_milestone'
        verbose_name = _('Milestone')
        verbose_name_plural = _('Milestones')

    def __unicode__(self):
        if self.language_code:
            return '{}: {}'.format(self.project, self.title)
        else:
            return '{}: {} (ID: {})'.format(self.project, self.__class__.__name__, self.pk)

    def __str__(self):
        if self.language_code:
            return '{}: {}'.format(self.project, self.title)
        else:
            return '{}: {} (ID: {})'.format(self.project, self.__class__.__name__, self.pk)


# Hint: Making existing fields translatable,
# see https://django-parler.readthedocs.io/en/latest/advanced/migrating.html
class WorkItem(TranslatableModel):
    translations = TranslatedFields(
        name=models.CharField(max_length=140),
        description=HTMLField(default='')
    )
    date = models.DateField(default=now)
    milestone = models.ForeignKey('Milestone', on_delete=models.CASCADE, related_name="workitems")
    state = models.IntegerField(choices=STATUS_CHOICES, default=0)

    class Meta:
        db_table = 'project_workitem'
        verbose_name = _('WorkItem')
        verbose_name_plural = _('WorkItems')

    def __unicode__(self):
        if self.language_code:
            return self.name
        else:
            return '{} (ID: {})'.format(self.__class__.__name__, self.pk)

    def __str__(self):
        if self.language_code:
            return self.name
        else:
            return '{} (ID: {})'.format(self.__class__.__name__, self.pk)
