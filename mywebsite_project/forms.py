# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

from django import forms
from mywebsite.utils.widgets import TinyMCEWidget
from parler.forms import TranslatableModelForm

from . import models


class ProjectAdminForm(TranslatableModelForm):
    description = forms.CharField(widget=TinyMCEWidget(
        attrs={'cols': 30, 'rows': 10}
    ))

    class Meta:
        model = models.Project
        exclude = {}

    def __init__(self, *args, **kwargs):
        super(ProjectAdminForm, self).__init__(*args, **kwargs)


class WorkItemAdminForm(TranslatableModelForm):
    description = forms.CharField(widget=TinyMCEWidget(
        attrs={'cols': 30, 'rows': 10}
    ))

    class Meta:
        model = models.WorkItem
        exclude = {}

    def __init__(self, *args, **kwargs):
        super(WorkItemAdminForm, self).__init__(*args, **kwargs)
