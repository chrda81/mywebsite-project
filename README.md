# MyWebsite-project

This is the project package of the modular WEB application for my business. It is written in Python/Django.

---

License agreement:

Copyright (C) 2018 dsoft-app-dev.de and friends.

This Program may be used by anyone in accordance with the terms of the
German Free Software License

The License may be obtained under http://www.d-fsl.org.

---

If you like my work, I would appreciate a donation. You can find several options on my website www.dsoft-app-dev.de at section crowdfunding. Thank you!

This application depends on some Python modules, which should be installed into the virtual environment the WEB application is running from within. For the development system use:

    $ workon <name of virtualenv>
    $ pip install -r requirements/development.txt

For the production system use:

    $ workon <name of virtualenv>
    $ pip install -r requirements/production.txt

Create your own copy of settings_secure.py and tweak the settings:

    $ cp test_website/settings/settings_secure.example.py test_website/settings/settings_secure.py

To generate a new SECRET_KEY use:

    $ function django_secret() { python -c "import random,string;print(''.join([random.SystemRandom().choice(\"{}{}{}\".format(string.ascii_letters, string.digits, string.punctuation)) for i in range(63)]).replace('\\'','\\'\"\\'\"\\''))"; }
    $ echo "DJANGO_SECRET_KEY='$(django_secret)'"

To create the database structure and group permissions use:

    $ ./manage.py migrate
    $ ./manage.py create_editors_permissions

Now collect all static files. But before running the command, you have to collect all node_modules from third party vendors:

    $ yarn

For the production system use:

    $ yarn --production

Then you can run the command:

    $ ./manage.py collectstatic

Rename "cookiecutter_template_dir" in templates/\*/home/base.html to your choice of string "template_dir".

To import sample data, use:

    $ ./manage.py loaddata fixtures/sample_data.json

To dump the whole database use:

    $ ./manage.py dumpdata --indent 2 > fixtures/dump.json

To dump a single database table use:

    $ ./manage.py dumpdata --indent 2 mywebsite_home.websitesettings > fixtures/dump_websettings.json

It is possible to exclude unneccessary tables during the database dump, e.g.:

    $ ./manage.py dumpdata --natural-foreign --natural-primary --indent 2 -e filebrowser -e reversion -e hitcount -e sessions -e admin -e captcha -e contenttypes -e auth -e dashboard -e mywebsite_members  > fixtures/sample_data.json

The app 'extendcmds' allows to create a superuser with a given password:

    $ ./manage.py createsuperuser --username <account name> --email <email> --password <secret password> --preserve --noinput

## Settings for mywebsite_home app

The mywebsite_home app represents the website with its index page. In order to get the correct pages for menu entries 'Crowdfunding', 'About', 'Disclosures' and 'Data privacy', you have to create 3 article pages in the mywebsite_blog app and name the slug field with the names 'crowdfunding', 'about-me', 'disclosures' and 'data-privacy'.

## Unit tests

Unit tests for Javascript is done by using QUnit, see https://qunitjs.com
To execute tests, you have 2 possibilities:

1. Run "python -m http.server" instead of "./manage.py runserver" from command line and browse http://localhost:8000/test/tests.html

or

2. Install QUnit (npm install -g qunit) and simply run it from command line "qunit"

All other unit tests can be run with

    $ tox

To test the cache scenario, you have to install "memcached" and run it with:

    $ memcached -vv

## More MyWebsite modules

-   [mywebsite_base](https://bitbucket.org/chrda81/mywebsite-base)
-   [mywebsite_celery](https://bitbucket.org/chrda81/mywebsite-celery)
-   [mywebsite_members](https://bitbucket.org/chrda81/mywebsite-members)

To be continued ...

## Build package

To build package via setup.py use:

    $ python setup.py sdist --formats=zip --dist-dir .tox/dist

To install this built package locally in your Python virtual environment, use e.g.:

    $ pip install --exists-action w .tox/dist/mywebsite_base_django-1.0.9.zip

## GIT pre-commit example

To change the build no. automatically, create a file '.git/pre-commit' with the following content:

    #!/bin/bash

    # extract version and write it to VERSION file before commit
    VERSION_NO=`cat package.json | python -c "import json,sys;obj=json.load(sys.stdin);print(obj['version']);"`

    echo $VERSION_NO > ./VERSION
    cp ./mywebsite_project/__init__.py ./mywebsite_project/__init__.py.bak
    sed "/__version__/s/.*/__version__ = '$VERSION_NO'/" ./mywebsite_project/__init__.py.bak > ./mywebsite_project/__init__.py
    rm ./mywebsite_project/__init__.py.bak

## VSCode custom configuration

Custum configuration files are stored in the .vscode folder under the project path. The file _settings.json_ contains
full path settings to programs, e.g. python, pep8 and so forth. It is recommended to exclude this file in git, using
_.gitignore_. Here is a template for _settings.json_:

    {
        "python.pythonPath": "<path to bin/python>",
        "python.linting.pep8Path": "<path to bin/pep8>",
        "python.linting.pep8Args": [
            "--max-line-length=119"
        ],
        "python.linting.pep8Enabled": true,
        "python.formatting.autopep8Path": "<path to bin/autopep8>",
        "python.formatting.autopep8Args": [
            "--max-line-length=119"
        ],
        "python.linting.pylintPath": "<path bin/pylint>",
        "python.linting.pylintArgs": [
            "--errors-only",
            "--load-plugins",
            "pylint_django"
        ],
        "python.linting.pylintEnabled": true,
        "html.format.enable": false,
        "eslint.enable": false,
        "editor.formatOnSave": true,
        "editor.rulers": [
            119,
            140
        ],
        "files.exclude": {
            "**/.git": true,
            "**/.svn": true,
            "**/.hg": true,
            "**/CVS": true,
            "**/.DS_Store": true,
            ".vscode": false,
            ".idea": true,
            "**/*,pyc": true,
            "*/__pycache__": true
        },
        "workbench.editor.enablePreview": false,
        "editor.find.globalFindClipboard": true,
        "search.globalFindClipboard": true,
        "editor.minimap.side": "left",
        "git.promptToSaveFilesBeforeCommit": true,
        "markdown-pdf.type": [
            "html",
        ],
        "markdown-pdf.convertOnSave": true,
        "markdown-pdf.outputDirectory": "mywebsite_home/templates/admin_doc",
        "markdown-pdf.convertOnSaveExclude": [
            "^(?!MANUAL.*\\.md$)",
        ],
        "markdown-pdf.includeDefaultStyles": false,
        "markdown-pdf.styles": [
            "mywebsite/static/mywebsite/assets_bundles/appStyle.css",
        ],
    }

## Commit message convention

We follow the [conventional commits specification](https://www.conventionalcommits.org/en) for our commit messages:

- `style`: style changes, e.g. add css code to a modules stylesheet section.
- `fix`: bug fixes, e.g. fix crash due to deprecated method.
- `feat`: new features, e.g. add new method to the module.
- `refactor`: code refactor, e.g. migrate from class components to hooks.
- `docs`: changes to documentation, e.g. add usage example for the module.
- `test`: adding or updating tests, eg add integration tests using detox.
- `chore`: tooling changes, e.g. change CI config.

You can also add a scope to your message, e.g.:

- `chore(deps)`: Update package @react-navigation/drawer to version 6.3.0
- `fix(navigation)`: Rework linking configuration
- `style(global)`: Rework responsive styles

# Change history

    1.2.0:

-   chore(deps): Upgraded package mywebsite-base-django to version 1.3.0

    1.1.1:

-   fix(deps): Upgraded package mywebsite-base-django to version 1.2.2.

    1.1.0:

-   fix(deps): Upgraded package mywebsite-base-django to version 1.2.1.
-   fix(deps): Handle RemovedInDjango40Warning and replace ugettext_lazy and ugettext

    1.0.7:

-   fix(deps): Upgraded package mywebsite-base-django to version 1.1.21.

    1.0.6:

-   Upgraded package mywebsite-base-django to version 1.1.18.

    1.0.5:

-   Upgraded package mywebsite-base-django to version 1.1.16.
-   Bugfix for unit tests.

    1.0.4:

-   Upgraded package mywebsite-base-django to version 1.1.12. Made depending changes.
-   Bugfix for ./manage.py createinitialrevisions

    1.0.3:

-   Upgraded package mywebsite-base-django to version 1.1.10. Made depending changes.
-   Modified mywebsite_project/**init**.py
-   Fixed OperationalError
-   Added translation for models.
-   Added admin model reorder.

    1.0.2:

-   Upgraded package mywebsite-base-django to version 1.1.2. Added missing static files. Made changes to webpack dependencies.

    1.0.1:

-   Upgraded package mywebsite-base-django to version 1.1.1. Made depending changes.
